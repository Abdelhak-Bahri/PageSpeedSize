PageSpeedSize :

test page speed and size using python requests library

dependencies:

python3, requests library

usage:

python test_speed.py "http://example.url/"


example:

abdelhak$ python test_speed.py "http://www.google.dz"
    time     | size (octets)
    0.129195  |  5126
    0.126076  |  5134
    0.179649  |  5134
    0.181609  |  5145
    0.144853  |  5168
    0.14973  |  5169
    0.132386  |  5130
    0.179902  |  5167
    0.125541  |  5163
    0.116642  |  5168
    average time:  0.14655829999999997
    average size:  5150.4