import requests
import sys
print('time     | size (octets)')
times,sizes = [],[]
for i in range(10):
    r = requests.get(sys.argv[1])
    time = r.elapsed.total_seconds()
    size = r.headers['content-length']
    times.append(time)
    sizes.append(int(size))
    print(time," | ", size)
print("average time: ",sum(times) / float(len(times)))
print("average size: ",sum(sizes) / float(len(sizes)))